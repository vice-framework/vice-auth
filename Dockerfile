# ==================================== BASE ====================================
ARG INSTALL_PYTHON_VERSION=${INSTALL_PYTHON_VERSION:-3.9}
ARG CURL_CA_BUNDLE=${CURL_CA_BUNDLE:-''}
FROM python:${INSTALL_PYTHON_VERSION}-slim-buster AS base

ENV PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_NO_INTERACTION=1 \
    CURL_CA_BUNDLE=$CURL_CA_BUNDLE

RUN apt-get update
RUN apt-get install -y curl gcc git

WORKDIR /app
RUN pip install poetry

RUN useradd -m vice
RUN chown -R vice:vice /app
USER vice

RUN python -m venv /var/tmp/venv

COPY pyproject.toml poetry.lock .
RUN poetry export -f requirements.txt --without-hashes | grep -v 'index-url' | /var/tmp/venv/bin/pip install -r /dev/stdin
COPY --chown=vice . .

# ================================= DEVELOPMENT ================================
FROM base AS development
RUN poetry export --dev -f requirements.txt --without-hashes | grep -v 'index-url' | /var/tmp/venv/bin/pip install -r /dev/stdin
EXPOSE 5000
CMD [ "poetry", "run", "flask", "run", "--host=0.0.0.0" ]

# ================================= PRODUCTION =================================
FROM base AS production
COPY supervisord.conf /etc/supervisor/supervisord.conf
COPY supervisord_programs /etc/supervisor/conf.d
EXPOSE 5000
ENTRYPOINT ["/bin/bash", "shell_scripts/supervisord_entrypoint.sh"]
CMD ["-c", "/etc/supervisor/supervisord.conf"]

# =================================== MANAGE ===================================
FROM base AS manage
COPY --from=development /var/tmp/venv /var/tmp/venv
ENTRYPOINT [ "poetry", "run", "flask" ]
