import datetime as dt
import secrets
import time
import uuid
from unittest.mock import ANY

import jwt
import pytest
from flask import current_app
from sqlalchemy import false
from sqlalchemy.orm.exc import NoResultFound

from tests.factories import AccountFactory
from vauth.auth import controller
from vauth.auth.controller import send_mail
from vauth.auth.models import Account, TokenType, WebToken, WebTokenType

TEST_EMAIL = "test@email.com"
TEST_PASSWORD = "password"
TEST_FIRST_NAME = "John"
TEST_LAST_NAME = "Doe"


def datetime_to_rfc1123(date):
    return date.strftime("%a, %d %b %Y %H:%M:%S GMT")  # RFC 1123


def decode_jwt(token):
    return jwt.decode(token, current_app.config["SECRET_KEY"], algorithms=["HS256"])


def encode_jwt(token):
    return jwt.encode(token, current_app.config["SECRET_KEY"], algorithm="HS256")


def assert_smtp_called(email=TEST_EMAIL, type="verify", token=ANY, origin=None, sent=True):
    if type == "verify":
        func = send_mail.send_verification_email
        args = (email, token, origin)
    elif type == "reset":
        func = send_mail.send_reset_email
        args = (email, token, origin)
    elif type == "registered":
        func = send_mail.send_already_registered_email
        args = (email, origin)
    else:
        return

    if sent:
        func.assert_called_once_with(*args)
    else:
        assert not func.called


def remove_none(data):
    return {k: v for k, v in data.items() if v is not None}


@pytest.fixture()
def registered_user(db):
    return AccountFactory(password=TEST_PASSWORD).save()


@pytest.mark.usefixtures("db", "_mock_smtp")
class TestRegistration:
    @staticmethod
    def send(
        testapp,
        email=TEST_EMAIL,
        password=TEST_PASSWORD,
        first_name=TEST_FIRST_NAME,
        last_name=TEST_LAST_NAME,
        status=201,
    ):
        return testapp.post_json(
            "/accounts/register",
            remove_none({"email": email, "password": password, "firstName": first_name, "lastName": last_name}),
            status=status,
        )

    def test_new_user(self, testapp):
        response = TestRegistration.send(testapp).json

        assert_smtp_called(
            type="verify",
            token=Account.query.filter_by(email=TEST_EMAIL)
            .one()
            .tokens.filter_by(type=TokenType.VERIFICATION)
            .one()
            .tid,
        )
        assert response == {"message": "Successfully registered."}

    def test_duplicate_email(self, testapp):
        TestRegistration.send(testapp)
        response = TestRegistration.send(testapp).json

        assert_smtp_called(type="registered")

        assert response == {"message": "Successfully registered."}

    def test_missing_data(self, testapp):
        expected_resp = {
            "code": 400,
            "message": "email, password, first_name, and last_name are required to " "register.",
            "name": "Bad Request",
        }

        response = TestRegistration.send(testapp, email=None, status=400).json
        assert response == expected_resp

        response = TestRegistration.send(testapp, password=None, status=400).json
        assert response == expected_resp

        response = TestRegistration.send(testapp, first_name=None, status=400).json
        assert response == expected_resp

        response = TestRegistration.send(testapp, last_name=None, status=400).json
        assert response == expected_resp


@pytest.mark.usefixtures("db", "_mock_smtp")
class TestForgotPassword:
    @staticmethod
    def send(testapp, email, status=200):
        return testapp.post_json(
            "/accounts/forgot-password",
            remove_none({"email": email}),
            status=status,
        )

    def test_successful(self, testapp, registered_user):
        expected_resp = {"message": "If the account exists, a recovery email will be sent."}
        response = TestForgotPassword.send(testapp, registered_user.email).json

        token = registered_user.tokens.filter_by(type=TokenType.RESET).one().tid
        assert_smtp_called(registered_user.email, type="reset", token=token)
        assert response == expected_resp

    def test_unregistered_user(self, testapp, registered_user):
        # Don't fail to prevent account enumeration
        TestForgotPassword.send(testapp, registered_user.email + "bogus")
        assert_smtp_called(sent=False)

    def test_missing_data(self, testapp):
        expected_resp = {"code": 400, "name": "Bad Request", "message": "Email is required to process this request."}
        response = TestForgotPassword.send(testapp, None, status=400).json

        assert response == expected_resp


@pytest.mark.usefixtures("db", "_mock_smtp")
class TestValidateResetToken:
    type = "validate"

    @staticmethod
    def send(testapp, token, password=None, status=200):
        return testapp.post_json(
            "/accounts/validate-reset-token",
            remove_none({"token": token}),
            status=status,
        )

    def test_successful(self, testapp, registered_user):
        expected_resp = {"message": "Token is valid."}

        TestForgotPassword.send(testapp, registered_user.email)
        token = registered_user.tokens.filter_by(type=TokenType.RESET).one().tid
        response = self.send(testapp, token).json

        assert response == expected_resp

    def test_expired(self, testapp, registered_user):
        new_password = "dummy"
        expected_resp = {"code": 401, "message": "Token has expired.", "name": "Unauthorized"}

        TestForgotPassword.send(testapp, registered_user.email)

        token = registered_user.tokens.filter_by(type=TokenType.RESET).one()
        token.update(expires_date=dt.datetime.utcnow())

        response = self.send(testapp, token.tid, new_password, status=401).json

        assert response == expected_resp
        assert Account.query.filter_by(email=registered_user.email).one().check_password(password=new_password) is False

    def test_revoked(self, testapp, registered_user):
        new_password = "dummy"
        expected_resp = {"code": 403, "name": "Forbidden", "message": "Token has been revoked."}

        TestForgotPassword.send(testapp, registered_user.email)

        token = registered_user.tokens.filter_by(type=TokenType.RESET).one()
        token.update(revoked=True)

        response = self.send(testapp, token.tid, new_password, status=403).json

        assert response == expected_resp
        assert Account.query.filter_by(email=registered_user.email).one().check_password(password=new_password) is False

    def test_invalid(self, testapp, registered_user):
        new_password = "dummy"
        expected_resp = {"code": 404, "name": "Not Found", "message": "Invalid reset token."}

        TestForgotPassword.send(testapp, registered_user.email)
        response = self.send(testapp, secrets.token_urlsafe(39), new_password, status=404).json

        assert response == expected_resp
        assert Account.query.filter_by(email=registered_user.email).one().check_password(password=new_password) is False

    def test_missing_data(self, testapp):
        if self.type == "validate":
            expected_resp = {
                "code": 400,
                "message": "token is required to complete the request.",
                "name": "Bad Request",
            }
        else:
            expected_resp = {"code": 400, "name": "Bad Request", "message": "reset token and password required."}

        response = self.send(testapp, None, "password", status=400).json

        assert response == expected_resp


@pytest.mark.usefixtures("db", "_mock_smtp")
class TestResetPassword(TestValidateResetToken):
    type = "reset"

    @staticmethod
    def send(testapp, token, password=None, status=200):
        return testapp.post_json(
            "/accounts/reset-password",
            remove_none({"token": token, "password": password}),
            status=status,
        )

    def test_successful(self, testapp, registered_user):
        new_password = "dummy"
        expected_resp = {"message": "Password reset successful, you can now log in."}

        TestForgotPassword.send(testapp, registered_user.email)
        token = registered_user.tokens.filter_by(type=TokenType.RESET).one().tid
        response = self.send(testapp, token, new_password).json

        assert_smtp_called(registered_user.email, "reset", token)
        assert response == expected_resp
        assert Account.query.filter_by(email=registered_user.email).one().check_password(password=new_password) is True


@pytest.mark.usefixtures("db", "_mock_smtp")
class TestVerifyEmail:
    @staticmethod
    def send(testapp, token, status=200):
        return testapp.post_json(
            "/accounts/verify-email",
            remove_none({"token": token}),
            status=status,
        )

    def test_successful(self, testapp):
        expected_resp = {"message": "Successfully validated email address."}
        TestRegistration.send(testapp)
        token = Account.query.filter_by(email=TEST_EMAIL).one().tokens.filter_by(type=TokenType.VERIFICATION).one().tid
        assert_smtp_called(type="verify", token=token)

        response = TestVerifyEmail.send(testapp, token).json

        assert response == expected_resp

    def test_expired(self, testapp):
        expected_resp = {"code": 401, "name": "Unauthorized", "message": "Token has expired."}
        TestRegistration.send(testapp)
        token = Account.query.filter_by(email=TEST_EMAIL).one().tokens.filter_by(type=TokenType.VERIFICATION).one()
        token.update(expires_date=dt.datetime.utcnow())

        response = TestVerifyEmail.send(testapp, token.tid, 401).json

        assert response == expected_resp

    def test_invalid(self, testapp, registered_user):
        expected_resp = {"code": 404, "name": "Not Found", "message": "Could not verify email."}
        TestRegistration.send(testapp)
        token = Account.query.filter_by(email=TEST_EMAIL).one().tokens.filter_by(type=TokenType.VERIFICATION).one().tid
        assert_smtp_called(type="verify", token=token)

        response = TestVerifyEmail.send(testapp, secrets.token_urlsafe(39), status=404).json

        assert response == expected_resp
        assert Account.query.filter_by(email=registered_user.email).one().verified_date is None

    def test_revoked(self, testapp, registered_user):
        expected_resp = {"code": 403, "name": "Forbidden", "message": "Token has been revoked."}
        TestRegistration.send(testapp)
        token = Account.query.filter_by(email=TEST_EMAIL).one().tokens.filter_by(type=TokenType.VERIFICATION).one()
        assert_smtp_called(type="verify", token=token.tid)

        token.update(revoked=True)
        response = TestVerifyEmail.send(testapp, token.tid, status=403).json

        assert response == expected_resp
        assert Account.query.filter_by(email=registered_user.email).one().verified_date is None

    def test_missing_data(self, testapp):
        expected_resp = {"code": 400, "name": "Bad Request", "message": "token is required to process this request."}
        TestRegistration.send(testapp)
        token = Account.query.filter_by(email=TEST_EMAIL).one().tokens.filter_by(type=TokenType.VERIFICATION).one().tid
        assert_smtp_called(type="verify", token=token)

        response = TestVerifyEmail.send(testapp, None, 400).json

        assert response == expected_resp


@pytest.mark.usefixtures("db", "_mock_smtp")
class TestAuthenticate:
    @staticmethod
    def send(testapp, email, password, status=200):
        return testapp.post_json(
            "/accounts/authenticate",
            remove_none({"email": email, "password": password}),
            status=status,
        )

    def test_successful(self, testapp, registered_user):
        expected_resp = controller.serialize_account(registered_user)
        expected_resp["created"] = datetime_to_rfc1123(expected_resp["created"])

        response = self.send(testapp, registered_user.email, TEST_PASSWORD).json

        # Decode jwt for analysis
        response["jwtToken"] = decode_jwt(response["jwtToken"])
        expected_resp["jwtToken"] = {
            **registered_user.web_tokens.filter_by(type=WebTokenType.ACCESS).one().to_jwt_keys(),
        }

        refresh_token = registered_user.web_tokens.filter_by(type=WebTokenType.REFRESH).one().to_jwt_keys()

        assert response == expected_resp
        assert decode_jwt(testapp.cookies["refreshToken"]) == refresh_token

    def test_bad_login(self, testapp, registered_user):
        expected_resp = {"code": 401, "message": "Bad login.", "name": "Unauthorized"}
        response = self.send(testapp, registered_user.email + "bad", TEST_PASSWORD, 401).json

        assert response == expected_resp
        with pytest.raises(KeyError):
            assert testapp.cookies["refreshToken"]

        response = self.send(testapp, registered_user.email, TEST_PASSWORD + "bad", 401).json

        assert response == expected_resp
        with pytest.raises(KeyError):
            assert testapp.cookies["refreshToken"]

    def test_revoked(self, testapp, registered_user):
        expected_resp = {"code": 401, "message": "Account has been disabled.", "name": "Unauthorized"}
        registered_user.update(enabled=False)
        response = self.send(testapp, registered_user.email, TEST_PASSWORD, 401).json

        assert response == expected_resp
        with pytest.raises(KeyError):
            assert testapp.cookies["refreshToken"]

    def test_missing_data(self, testapp, registered_user):
        expected_resp = {
            "code": 400,
            "name": "Bad Request",
            "message": "email and password are required to process this request.",
        }
        response = self.send(testapp, None, TEST_PASSWORD, 400).json
        assert response == expected_resp

        response = self.send(testapp, registered_user.email, None, 400).json
        assert response == expected_resp


@pytest.mark.usefixtures("db", "_mock_smtp")
class TestRefreshToken:
    @staticmethod
    def send(testapp, token, status=200):
        return testapp.post(
            "/accounts/refresh-token",
            headers={"Authorization": f"Bearer {token}"},
            status=status,
        )

    def test_successful(self, testapp, registered_user):
        expected_resp = controller.serialize_account(registered_user)
        expected_resp["created"] = datetime_to_rfc1123(expected_resp["created"])

        TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD)
        response = self.send(testapp, testapp.cookies["refreshToken"]).json

        # Decode jwt for analysis
        response["jwtToken"] = decode_jwt(response["jwtToken"])
        expected_resp["jwtToken"] = {
            **registered_user.web_tokens.filter(WebToken.type == WebTokenType.ACCESS, WebToken.revoked == false())
            .one()
            .to_jwt_keys(),
        }

        refresh_token = (
            registered_user.web_tokens.filter(WebToken.type == WebTokenType.REFRESH, WebToken.revoked == false())
            .one()
            .to_jwt_keys()
        )

        assert response == expected_resp
        assert decode_jwt(testapp.cookies["refreshToken"]) == refresh_token

    def test_malformed_header(self, testapp, registered_user):
        expected_resp = {
            "message": "Missing 'Bearer' type in 'Authorization' header. Expected 'Authorization: Bearer <JWT>'"
        }
        TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD)
        response = testapp.post(
            "/accounts/refresh-token", headers={"Authorization": f'Bearer{testapp.cookies["refreshToken"]}'}, status=401
        ).json

        assert response == expected_resp

    def test_expired(self, testapp, registered_user):
        expected_resp = {"message": "Token has expired"}
        TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD)
        time.sleep(current_app.config["JWT_REFRESH_TOKEN_EXPIRES"] + 1)

        response = self.send(testapp, testapp.cookies["refreshToken"], 401).json

        assert response == expected_resp

    def test_revoked(self, testapp, registered_user):
        expected_resp = {"message": "Token has been revoked"}
        TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD)
        registered_user.web_tokens.filter_by(type=WebTokenType.REFRESH).one().update(revoked=True)

        response = self.send(testapp, testapp.cookies["refreshToken"], 401).json

        assert response == expected_resp

    def test_invalid(self, testapp, registered_user):
        expected_resp = {"message": "Token has been revoked"}
        TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD)
        token = registered_user.web_tokens.filter_by(type=WebTokenType.REFRESH).one()
        token.update(revoked=True)

        wrong_token_keys = registered_user.web_tokens.filter_by(type=WebTokenType.REFRESH).one().to_jwt_keys()
        wrong_token_keys["jti"] = str(uuid.uuid4())
        wrong_token = encode_jwt(wrong_token_keys)

        response = self.send(testapp, wrong_token, 401).json

        assert response == expected_resp

    def test_missing_data(self, testapp):
        expected_resp = {"message": "Missing Authorization Header"}
        response = testapp.post("/accounts/refresh-token", status=401).json

        assert response == expected_resp

    def test_already_active_token(self, testapp, registered_user):
        self.test_successful(testapp, registered_user)
        self.test_successful(testapp, registered_user)


@pytest.mark.usefixtures("db", "_mock_smtp")
class TestRevokeToken:
    @staticmethod
    def send(testapp, token, status=200):
        return testapp.post(
            "/accounts/revoke-token",
            headers={"Authorization": f"Bearer {token}"},
            status=status,
        )

    def test_successful(self, testapp, registered_user):
        expected_resp = {"message": "Token(s) revoked."}
        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
        response = self.send(testapp, response["jwtToken"]).json

        assert response == expected_resp
        assert registered_user.web_tokens.filter_by(type=WebTokenType.ACCESS).one().revoked is True
        assert registered_user.web_tokens.filter_by(type=WebTokenType.REFRESH).one().revoked is True

    def test_expired(self, testapp, registered_user):
        expected_resp = {"message": "Token has expired"}
        TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD)
        time.sleep(current_app.config["JWT_REFRESH_TOKEN_EXPIRES"] + 1)

        response = self.send(testapp, testapp.cookies["refreshToken"], 401).json

        assert response == expected_resp

    def test_revoked(self, testapp, registered_user):
        expected_resp = {"message": "Token has been revoked"}
        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
        registered_user.web_tokens.filter_by(type=WebTokenType.ACCESS).one().update(revoked=True)

        response = self.send(testapp, response["jwtToken"], 401).json

        assert response == expected_resp

    def test_invalid(self, testapp, registered_user):
        expected_resp = {"message": "Token has been revoked"}
        TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD)
        token = registered_user.web_tokens.filter_by(type=WebTokenType.ACCESS).one()
        token.update(revoked=True)

        wrong_token_keys = registered_user.web_tokens.filter_by(type=WebTokenType.ACCESS).one().to_jwt_keys()
        wrong_token_keys["jti"] = str(uuid.uuid4())
        wrong_token = encode_jwt(wrong_token_keys)

        response = self.send(testapp, wrong_token, 401).json

        assert response == expected_resp

    def test_missing_data(self, testapp):
        expected_resp = {"message": "Missing Authorization Header"}
        response = testapp.post("/accounts/revoke-token", status=401).json

        assert response == expected_resp


@pytest.mark.usefixtures("db", "_mock_smtp")
class TestAccounts:
    @staticmethod
    def send(testapp, token, send_type="post", id=None, status=200, **kwargs):
        id = "" if id is None else f"/{id}"

        if send_type == "post":
            sender = testapp.post_json
        elif send_type == "put":
            sender = testapp.put_json
        elif send_type == "get":
            sender = testapp.get
        elif send_type == "delete":
            sender = testapp.delete_json
        else:
            return

        return sender(
            f"/accounts{id}",
            remove_none(kwargs),
            headers={"Authorization": f"Bearer {token}"},
            status=status,
        )

    def test_successful_get_all(self, testapp, registered_user):
        registered_user.update(admin=True)
        user1 = controller.serialize_account(registered_user)
        user1["created"] = datetime_to_rfc1123(user1["created"])

        user2 = controller.serialize_account(AccountFactory().save())
        user2["created"] = datetime_to_rfc1123(user2["created"])

        expected_resp = [user1, user2]

        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json

        response = self.send(testapp, response["jwtToken"], "get").json

        assert response == expected_resp

    def test_successful_get_one(self, testapp, registered_user):
        registered_user.update(admin=True)

        expected_resp = controller.serialize_account(AccountFactory().save())
        expected_resp["created"] = datetime_to_rfc1123(expected_resp["created"])

        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json

        response = self.send(testapp, response["jwtToken"], "get", 2).json

        assert response == expected_resp

    def test_successful_create(self, testapp, registered_user):
        registered_user.update(admin=True)

        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json

        new_user = {"email": "email", "firstName": "first", "lastName": "last"}
        response = self.send(testapp, response["jwtToken"], "post", **new_user, password="password").json
        user = Account.query.filter_by(email="email").one()
        expected_resp = {
            **new_user,
            "created": datetime_to_rfc1123(user.created_date),
            "id": user.id,
            "isVerified": True,
            "role": "User",
            "title": "",
        }

        assert response == expected_resp

    def test_successful_update(self, testapp, registered_user):
        registered_user.update(admin=True)
        AccountFactory().save()

        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
        new_info = {"email": "email", "firstName": "first", "lastName": "last"}
        response = self.send(testapp, response["jwtToken"], "put", id=2, **new_info, password="password1").json
        user = Account.query.filter_by(email="email").one()

        assert response == {
            **new_info,
            "created": datetime_to_rfc1123(user.created_date),
            "id": user.id,
            "isVerified": False,
            "role": "User",
            "title": "",
        }
        assert user.check_password("password1") is True

    def test_successful_delete(self, testapp, registered_user):
        registered_user.update(admin=True)
        new_user = AccountFactory().save()

        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
        self.send(testapp, response["jwtToken"], "delete", id=2)

        with pytest.raises(NoResultFound):
            Account.query.filter_by(email=new_user.email).one()

    def test_get_permissions(self, testapp, registered_user):
        """User can get self, admin can get anyone. Admin is tested above."""
        AccountFactory().save()
        AccountFactory(admin=True).save()

        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
        expected_resp = controller.serialize_account(registered_user)
        expected_resp["created"] = datetime_to_rfc1123(expected_resp["created"])
        response = self.send(testapp, response["jwtToken"], "get", 1).json
        assert response == expected_resp

        # Accessing other users should fail
        expected_resp = {"code": 401, "name": "Unauthorized", "message": "Account is not authorized to use this API."}
        for id in [2, 3]:
            response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
            response = self.send(testapp, response["jwtToken"], "get", id, status=401).json
            assert response == expected_resp

    def test_post_permissions(self, testapp, registered_user):
        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
        expected_resp = {"code": 401, "name": "Unauthorized", "message": "Account is not authorized to use this API."}
        response = self.send(testapp, response["jwtToken"], "post", status=401).json
        assert response == expected_resp

    def test_put_permissions(self, testapp, registered_user):
        AccountFactory().save()
        AccountFactory(admin=True).save()

        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
        expected_resp = controller.serialize_account(registered_user)
        expected_resp["created"] = datetime_to_rfc1123(expected_resp["created"])
        response = self.send(testapp, response["jwtToken"], "put", 1, firstName="John", lastName="Wick").json
        expected_resp.update({"firstName": "John", "lastName": "Wick"})
        assert response == expected_resp

        # Accessing other users should fail
        expected_resp = {"code": 401, "name": "Unauthorized", "message": "Account is not authorized to use this API."}
        for id in [2, 3]:
            response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
            response = self.send(testapp, response["jwtToken"], "put", id, status=401).json
            assert response == expected_resp

    def test_delete_permissions(self, testapp, registered_user):
        AccountFactory().save()
        AccountFactory(admin=True).save()

        # Accessing other users should fail
        expected_resp = {"code": 401, "name": "Unauthorized", "message": "Account is not authorized to use this API."}
        for id in [2, 3]:
            response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
            response = self.send(testapp, response["jwtToken"], "delete", id, status=401).json
            assert response == expected_resp

        response = TestAuthenticate.send(testapp, registered_user.email, TEST_PASSWORD).json
        expected_resp = {"message": "Successfully removed account."}
        response = self.send(testapp, response["jwtToken"], "delete", 1, firstName="John", lastName="Wick").json
        assert response == expected_resp
