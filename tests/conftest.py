# -*- coding: utf-8 -*-
"""Defines fixtures available to all tests."""

import logging

import pytest
from webtest import TestApp

from vauth.app import create_app
from vauth.database import db as _db

from .factories import AccountFactory


@pytest.fixture()
def app():
    """Create application for the tests."""
    _app = create_app("tests.settings")
    _app.logger.setLevel(logging.CRITICAL)
    ctx = _app.test_request_context()
    ctx.push()

    yield _app

    ctx.pop()


@pytest.fixture()
def testapp(app):
    """Create Webtest app."""
    return TestApp(app)


@pytest.fixture()
def db(app):
    """Create database for the tests."""
    _db.app = app
    with app.app_context():
        _db.create_all()

    yield _db

    # Explicitly close DB connection
    _db.session.close()
    _db.drop_all()


@pytest.fixture()
def account(db):
    """Create auth for the tests."""
    account = AccountFactory(password="myprecious")
    db.session.commit()
    return account


@pytest.fixture()
def _mock_smtp(mocker):
    mocker.patch("vauth.auth.controller.send_mail.send_verification_email")
    mocker.patch("vauth.auth.controller.send_mail.send_reset_email")
    mocker.patch("vauth.auth.controller.send_mail.send_already_registered_email")
