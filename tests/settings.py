"""Settings module for test app."""
import datetime as dt

ENV = "development"
TESTING = True
SQLALCHEMY_DATABASE_URI = "sqlite://"
SECRET_KEY = "not-so-secret-in-tests"  # noqa
BCRYPT_LOG_ROUNDS = 4  # For faster tests; needs at least 4 to avoid "ValueError: Invalid rounds"
CACHE_TYPE = "simple"  # Can be "memcached", "redis", etc.
SQLALCHEMY_TRACK_MODIFICATIONS = False
JWT_IDENTITY_CLAIM = "sub"
JWT_ACCESS_TOKEN_EXPIRES = 1
JWT_REFRESH_TOKEN_EXPIRES = 1
JWT_ERROR_MESSAGE_KEY = "message"

# General
PLATFORM_NAME = "Vice"
VERIFY_EMAIL_EXPIRY = dt.timedelta(days=1)
RESET_PASSWORD_EXPIRY = dt.timedelta(days=1)

# SMTP Settings
SMTP_ENABLE = True
SMTP_HOST = "No host"
SMTP_PORT = 587
SMTP_USERNAME = "username"
SMTP_PASSWORD = "password"  # noqa
