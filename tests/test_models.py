# -*- coding: utf-8 -*-
"""Model unit tests."""
import datetime as dt
import uuid

import jwt
import pytest
from flask import current_app
from sqlalchemy import true
from sqlalchemy.exc import IntegrityError, InvalidRequestError

from vauth.auth.models import Account, MetaToken, Role, Token, TokenType, WebToken, WebTokenType

from .factories import AccountFactory, RoleFactory, TokenFactory, WebTokenFactory


@pytest.fixture()
def generate_token():
    def generator(email):
        now = dt.datetime.utcnow()
        token_data = {
            "iat": now,
            "nbf": now,
            "jti": str(uuid.uuid4()),
            "type": "access",
            "exp": now + dt.timedelta(seconds=60),
            "sub": email,
        }

        algorithm = "HS256"
        encoded_token = jwt.encode(token_data, current_app.config["SECRET_KEY"], algorithm=algorithm)

        return encoded_token, token_data

    return generator


@pytest.mark.usefixtures("db")
class TestRole:
    """Role tests."""

    def test_unique_name_constraint(self):
        """Ensure Role requires that each role name be unique."""
        role = RoleFactory().save()

        role = Role(name=role.name)
        with pytest.raises(IntegrityError):
            role.save()

    def test_account_relationship(self):
        """Test that Role creates a linkage to Account."""
        role = RoleFactory()
        account = AccountFactory()
        account.roles.append(role)
        account.save()

        assert account == role.accounts[0]
        assert account.roles[0] == role

    def test_not_nullable(self):
        """Ensure non-nullable properties remain that way."""
        role = Role(name=None)

        with pytest.raises(IntegrityError):
            role.save()


@pytest.mark.usefixtures("db")
class TestAccount:
    """Account tests."""

    def test_relationsips(self):
        role1 = RoleFactory().save()
        role2 = RoleFactory().save()
        wtoken1 = WebTokenFactory()
        wtoken2 = WebTokenFactory()
        token1 = TokenFactory()
        token2 = TokenFactory()
        account = AccountFactory(roles=[role1, role2], web_tokens=[wtoken1, wtoken2], tokens=[token1, token2]).save()

        assert account.roles.order_by(Role.id).all()[0] == role1
        assert account.roles.order_by(Role.id).all()[1] == role2

        assert account.web_tokens.order_by(WebToken.id).all()[0] == wtoken1
        assert account.web_tokens.order_by(WebToken.id).all()[1] == wtoken2

        assert account.tokens.order_by(Token.id).all()[0] == token1
        assert account.tokens.order_by(Token.id).all()[1] == token2

    def test_not_nullable(self):
        with pytest.raises(IntegrityError):
            AccountFactory(email=None).save()

        with pytest.raises(InvalidRequestError):
            AccountFactory(created_date=None).save()

        with pytest.raises(InvalidRequestError):
            AccountFactory(updated_date=None).save()

        with pytest.raises(InvalidRequestError):
            AccountFactory(first_name=None).save()

        with pytest.raises(InvalidRequestError):
            AccountFactory(last_name=None).save()

        with pytest.raises(InvalidRequestError):
            AccountFactory(enabled=None).save()

        with pytest.raises(InvalidRequestError):
            AccountFactory(admin=None).save()

    def test_nullable(self):
        account = AccountFactory(verified_date=None, password_reset_date=None)
        account.password = None
        account.save()

        assert account.password is None
        assert account.verified_date is None
        assert account.password_reset_date is None

    def test_defaults(self):
        account = Account(email="email", password="password", first_name="foo", last_name="bar")
        account.save()

        assert account.created_date < dt.datetime.utcnow()
        assert account.updated_date < dt.datetime.utcnow()
        assert account.enabled is True
        assert account.admin is False
        assert account.verified_date is None

    def test_get_by_id(self):
        """Get account by ID."""
        account = AccountFactory()
        account.save()

        retrieved = Account.get_by_id(account.id)
        assert retrieved == account

    def test_check_password(self):
        """Check password."""
        account = AccountFactory(password="foobarbaz123")
        assert account.check_password("foobarbaz123") is True
        assert account.check_password("barfoobaz") is False

    def test_full_name(self):
        """Account full name."""
        account = AccountFactory(first_name="Foo", last_name="Bar")
        assert account.full_name == "Foo Bar"

    def test_roles(self):
        """Add a role to a account."""
        role = Role(name="admin")
        role.save()
        account = AccountFactory()
        account.roles.append(role)
        account.save()
        assert role in account.roles

    def test_tokens(self):
        """Add a token to a account."""
        account = AccountFactory()
        account.save()
        token = WebToken(account_id=account.id, jti=str(uuid.uuid4()), type=WebTokenType.REFRESH)
        token.save()
        account = AccountFactory()
        account.web_tokens.append(token)
        account.save()
        assert token in account.web_tokens


@pytest.mark.usefixtures("db")
class TestToken:
    """Token tests."""

    def test_relationships(self):
        account1 = AccountFactory().save()
        token = TokenFactory(account=account1).save()

        assert token.account == account1

    def test_not_nullable(self, db):
        """Ensure non-nullable properties remain that way."""
        account = AccountFactory().save()
        with pytest.raises(IntegrityError):
            Token(tid=None, account=account).save()

    def test_defaults(self):
        """Regression test DB default values."""
        account = AccountFactory().save()
        token = Token(account=account, type=TokenType.RESET).save()

        assert token.tid

    def test_token_type_constraint(self, db):
        """Ensure that one token type is allowed per Account [Reset, Verification]."""
        account1 = AccountFactory().save()
        account2 = AccountFactory().save()

        TokenFactory(account=account1).save()
        TokenFactory(account=account1).save()

        TokenFactory(account=account2).save()
        TokenFactory(account=account2).save()

        with pytest.raises(IntegrityError):
            TokenFactory(account=account1).save()

        db.session.rollback()

        with pytest.raises(IntegrityError):
            TokenFactory(account=account2).save()


@pytest.mark.usefixtures("db")
class TestWebToken:
    """WebToken tests."""

    def test_relationships(self):
        account1 = AccountFactory().save()
        token = WebTokenFactory(account=account1).save()

        assert token.account == account1

    def test_not_nullable(self, db):
        with pytest.raises(IntegrityError):
            WebToken(jti=None).save()

        db.session.rollback()

        with pytest.raises(IntegrityError):
            WebToken(not_before_date=None).save()

    def test_nullable(self):
        account = AccountFactory().save()
        token = WebTokenFactory(revoke_cause=None, account=account).save()
        assert token.revoke_cause is None

        token = WebTokenFactory(next_id=None, account=account).save()
        assert token.next_id is None

        token = WebTokenFactory(next=None, account=account).save()
        assert token.next is None

    def test_defaults(self):
        pass

    def test_from_encoded_token(self, generate_token):
        account = AccountFactory()
        account.save()
        encoded_token, token_data = generate_token(account.email)
        token = WebToken.from_encoded_token(encoded_token)

        assert token.jti == token_data["jti"]
        assert token.type == WebTokenType(token_data["type"])
        assert token.expires_date == token_data["exp"].replace(microsecond=0)
        assert token.revoked is False

    def test_is_token_revoked(self, generate_token):
        account = AccountFactory().save()
        encoded_token, token_data = generate_token(account.email)
        token = WebToken.from_encoded_token(encoded_token)
        token.revoked = True
        token.save()
        assert WebToken.is_token_revoked(token_data)


@pytest.mark.usefixtures("db")
class TestMetaToken:
    def test_relationships(self):
        account = AccountFactory().save()
        token = MetaToken(revoker=account)

        assert token.revoker == account

    def test_not_nullable(self, db):
        token = MetaToken().save()

        token.revoked = None
        with pytest.raises(IntegrityError):
            token.save()

        db.session.rollback()

        token.expires_date = None
        with pytest.raises(IntegrityError):
            token.save()

        db.session.rollback()

        token.issue_date = None
        with pytest.raises(IntegrityError):
            token.save()

    def test_nullable(self):
        token = MetaToken(revoked_date=None, revoker_ip=None).save()

        assert token.revoked_date is None
        assert token.revoker is None

    def test_defaults(self):
        token = MetaToken().save()

        assert token.revoked is False
        assert token.expires_date < dt.datetime.utcnow()

    def test_expired(self):
        """Ensure the expired property works."""
        token = MetaToken().save()

        assert token.expired is True

        token.expires_date = dt.datetime.now() + dt.timedelta(days=1)
        token.save()
        assert token.expired is False

    def test_valid(self):
        token = MetaToken(revoked=True).save()
        assert token.valid is False

        token.update(revoked=False)
        assert token.valid is False

        token.update(expires_date=dt.datetime.max)
        assert token.valid is True

        token.update(revoked=True)
        assert token.valid is False

    def test_expired_valid_hybrid_properties(self, db):
        """Check that expired and valid evaluate correctly in SQL."""
        token1 = MetaToken.create()
        token2 = MetaToken.create(expires_date=dt.datetime.max)
        token3 = MetaToken.create(expires_date=dt.datetime.max, revoked=True)

        db.session.commit()

        assert token1.expired
        assert token1.valid is False
        assert token1.revoked is False

        assert token2.expired is False
        assert token2.valid is True
        assert token2.revoked is False

        assert token3.expired is False
        assert token3.valid is False
        assert token3.revoked is True

        valid_token = MetaToken.query.filter(MetaToken.valid == true()).one()
        assert valid_token == token2

        expired_token = MetaToken.query.filter(MetaToken.expired == true()).one()
        assert expired_token == token1
