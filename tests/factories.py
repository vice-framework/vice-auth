# -*- coding: utf-8 -*-
"""Factories to help in tests."""
import secrets
import uuid

from factory import Faker, Iterator, PostGenerationMethodCall, post_generation  # factory-boy
from factory.alchemy import SQLAlchemyModelFactory

from vauth.auth.models import Account, RevokeCause, Role, Token, TokenType, WebToken, WebTokenType
from vauth.database import db


class BaseFactory(SQLAlchemyModelFactory):
    """Base factory."""

    class Meta:
        """Factory configuration."""

        abstract = True
        sqlalchemy_session = db.session


class AccountFactory(BaseFactory):
    """Account factory."""

    email = Faker("email")
    password = PostGenerationMethodCall("set_password", "example")
    enabled = True
    first_name = Faker("first_name")
    last_name = Faker("last_name")

    @post_generation
    def roles(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for role in extracted:
                self.roles.append(role)

    @post_generation
    def web_tokens(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for token in extracted:
                self.web_tokens.append(token)

    @post_generation
    def tokens(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for token in extracted:
                self.tokens.append(token)

    class Meta:
        """Factory configuration."""

        model = Account


class RoleFactory(BaseFactory):
    name = Faker("job")

    class Meta:
        """Factory configuration."""

        model = Role


class WebTokenFactory(BaseFactory):
    jti = str(uuid.uuid4())
    type = Iterator([WebTokenType.REFRESH, WebTokenType.ACCESS])
    revoke_cause = Iterator([RevokeCause.REFRESH, RevokeCause.USER, None])

    class Meta:
        """Factory configuration."""

        model = WebToken


class TokenFactory(BaseFactory):
    tid = secrets.token_urlsafe(39)
    type = Iterator([TokenType.VERIFICATION, TokenType.RESET])

    class Meta:
        """Factory configuration."""

        model = Token
