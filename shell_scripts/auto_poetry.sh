#!/usr/bin/env sh

auto_poetry_shell() {
    if [ -f "pyproject.toml" ] ; then
        source "$(poetry env info -p)/bin/activate"
    fi
}
