#!/usr/bin/env sh
set -e

source ./shell_scripts/auto_poetry.sh
auto_poetry_shell

if [ $# -eq 0 ] || [ "${1#-}" != "$1" ]; then
  set -- supervisord "$@"
fi

exec "$@"
