.. highlight:: bash

=====================================
Installation (NOT CURRENTLY POSSIBLE)
=====================================


Stable release
--------------

To install Vice, run this command in your terminal:

.. code-block:: console

    $ pip install vice

This is the preferred method to install Vice, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From source
-----------

The source for Vice can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://gitlab.com/vice-framework/vice-auth.git

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/vice-framework/vice-auth/-/releases

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ pip install .

.. _Gitlab repo: https://gitlab.com/vice-framework/vice-auth
.. _tarball: https://gitlab.com/vice-framework/vice-auth/-/releases
