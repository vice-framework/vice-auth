# -*- coding: utf-8 -*-
"""Auth views."""
import functools
from typing import Callable

from flask import Blueprint, jsonify, request
from flask.views import MethodView
from flask_jwt_extended import get_jwt_identity, jwt_required

from vauth.auth import controller
from vauth.exceptions import BadRequest, ServiceUnavailableError

blueprint = Blueprint("auth", __name__)


def check_if_admin_required(account_id: int) -> None:
    """
    Compare provided account ID against JWT identity to determine if admin is required.

    If JWT ID matches the resource owner ID, no admin is required.

    Args:
        account_id: Account owner ID

    Returns:
        None
    """
    if get_jwt_identity() != controller.get_account_by_id(account_id).email:
        controller.require_admin(get_jwt_identity())


def require_admin(func: Callable) -> Callable:
    """
    Require admin status for requesting user identified through a JWT access token.

    Args:
        func: Function to decorate

    Returns:
        Decorator
    """

    @jwt_required()
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        jwt_user_id = get_jwt_identity()

        controller.require_admin(jwt_user_id)

        return func(*args, **kwargs)

    return wrapper


class AuthenticateAPI(MethodView):
    """User Login Resource."""

    @staticmethod
    def post():
        """
        Authenticate user using email and password and issue access and refresh tokens.

        Returns:
            User details + tokens
        """
        data = request.get_json()
        try:
            refresh_token, access_token, account_details = controller.authenticate(
                data["email"], data["password"], request.remote_addr
            )
        except KeyError as ex:
            raise BadRequest("email and password are required to process this request.") from ex

        account_details["jwtToken"] = access_token
        out = jsonify(account_details)
        out.set_cookie("refreshToken", refresh_token, httponly=True, samesite="Lax")

        return out, 200


class RegisterAPI(MethodView):
    """User Registration Resource."""

    @staticmethod
    def post():
        """
        Register user account with API and send an email verification token.

        Returns:
            Success if operation completes
        """
        post_data = request.get_json()

        try:
            controller.register(
                post_data["email"],
                post_data["password"],
                post_data["firstName"],
                post_data["lastName"],
                request.environ.get("HTTP_ORIGIN", None),
            )
        except (TypeError, KeyError) as ex:
            raise BadRequest("email, password, first_name, and last_name are required to register.") from ex
        except ServiceUnavailableError:
            pass

        return {"message": "Successfully registered."}, 201


class VerifyEmailAPI(MethodView):
    """Verify user email address."""

    @staticmethod
    def post():
        """
        Verify email exists by validating verify-token.

        Returns:
            Success if operation completes
        """
        post_data = request.get_json()

        try:
            controller.verify_email(post_data["token"])
        except KeyError as ex:
            raise BadRequest("token is required to process this request.") from ex

        return {"message": "Successfully validated email address."}, 200


class ForgotPasswordAPI(MethodView):
    """Generate and email a password reset token."""

    @staticmethod
    def post():
        """
        Generate and email a password reset token.

        Returns:
            Success
        """
        post_data = request.get_json()

        try:
            controller.forgot_password(post_data["email"], request.environ.get("HTTP_ORIGIN", None))
        except KeyError as ex:
            raise BadRequest("Email is required to process this request.") from ex

        return {"message": "If the account exists, a recovery email will be sent."}, 200


class ResetPasswordAPI(MethodView):
    """Reset password using a reset token."""

    @staticmethod
    def post():
        """
        Reset password using a reset token.

        Returns:
            Success if operation completes
        """
        post_data = request.get_json()
        try:
            controller.reset_password(post_data["token"], post_data["password"])
        except KeyError as ex:
            raise BadRequest("reset token and password required.") from ex

        return {"message": "Password reset successful, you can now log in."}, 200


class ValidateResetTokenAPI(MethodView):
    """Validate reset token."""

    @staticmethod
    def post():
        """
        Validate reset token.

        Returns:
            Success if token is valid
        """
        post_data = request.get_json()
        try:
            controller.validate_reset_token(post_data["token"])
        except KeyError as ex:
            raise BadRequest("token is required to complete the request.") from ex

        return {"message": "Token is valid."}, 200


class AccountsAPI(MethodView):
    """Account creation and viewing API."""

    @staticmethod
    @require_admin
    def get():
        """
        Get all user accounts.

        Must be an admin.

        Returns:
            Success if operation completes
        """
        accounts = controller.get_accounts()
        out = jsonify([controller.serialize_account(account) for account in accounts])

        return out, 200

    @staticmethod
    @require_admin
    def post():
        """
        Create a user account.

        Must be an admin.

        Returns:
            Success if operation completes
        """
        post_data = request.get_json()

        account = controller.create_account(
            post_data["email"],
            post_data["password"],
            post_data["firstName"],
            post_data["lastName"],
        )

        out = jsonify(controller.serialize_account(account))
        return out, 200


class AccountAPI(MethodView):
    """Account management API."""

    @staticmethod
    @jwt_required()
    def put(account_id):
        """
        Update an account.

        User must own account or be an admin.

        Args:
            account_id: ID of account to fetch

        Returns:
            Success if operation completes
        """
        check_if_admin_required(account_id)
        post_data = request.get_json()
        data = {
            "first_name": post_data.get("firstName", None),
            "last_name": post_data.get("lastName", None),
            "email": post_data.get("email", None),
            "admin": post_data.get("role", None) == "Admin",
            "created_date": post_data.get("created", None),
            "verified_date": post_data.get("isVerified", None),
            "password": post_data.get("password", None),
        }
        data = {k: v for k, v in data.items() if v is not None}
        account = controller.update_account(account_id, data)

        return jsonify(controller.serialize_account(account))

    @staticmethod
    @jwt_required()
    def get(account_id):
        """
        Get user account details.

        User must own account or be an admin.

        Args:
            account_id: ID of account to fetch

        Returns:
            Success if operation completes
        """
        check_if_admin_required(account_id)
        account = controller.get_account_by_id(account_id)
        return jsonify(controller.serialize_account(account))

    @staticmethod
    @jwt_required()
    def delete(account_id):
        """
        Delete user account.

        User must own account or be an admin.

        Args:
            account_id: ID of account to delete

        Returns:
            Success if operation completes
        """
        check_if_admin_required(account_id)
        controller.delete_account(account_id)
        return {"message": "Successfully removed account."}, 200


class RefreshTokenAPI(MethodView):
    """User Login Resource."""

    @staticmethod
    @jwt_required(refresh=True)
    def post():
        """
        Create new refresh and access tokens for the given user.

        Returns:
            Success if operation completes
        """
        user = get_jwt_identity()
        refresh_token, access_token, account_details = controller.refresh_token(user, request.remote_addr)

        account_details["jwtToken"] = access_token
        out = jsonify(account_details)
        out.set_cookie("refreshToken", refresh_token, httponly=True, samesite="Lax")

        return out, 200


class RevokeTokenAPI(MethodView):
    """Logout Resource."""

    @staticmethod
    @jwt_required()
    def post():
        """
        Revoke current user's tokens.

        Returns:
            Success if operation completes
        """
        requesting_user = get_jwt_identity()
        # check_if_admin_required()
        # post_data = request.get_json()  # TODO: Allow an admin to revoke a different token
        controller.revoke_token(requesting_user, request.remote_addr)

        return {"message": "Token(s) revoked."}, 200


# define the API resources
registration_view = RegisterAPI.as_view("register_api")
authentication_view = AuthenticateAPI.as_view("authentication_api")
refresh_token_view = RefreshTokenAPI.as_view("refresh_token_api")
revoke_token_view = RevokeTokenAPI.as_view("revoke_token_api")
verify_email_view = VerifyEmailAPI.as_view("verify_email_api")
forgot_password_view = ForgotPasswordAPI.as_view("forgot_password_view")
reset_password_view = ResetPasswordAPI.as_view("reset_password_view")
validate_reset_token_view = ValidateResetTokenAPI.as_view("validate_reset_token_view")
accounts_view = AccountsAPI.as_view("accounts_view")
account_view = AccountAPI.as_view("account_view")

# add Rules for API Endpoints
blueprint.add_url_rule("/accounts/register", view_func=registration_view, methods=["POST"])
blueprint.add_url_rule("/accounts/authenticate", view_func=authentication_view, methods=["POST"])
blueprint.add_url_rule("/accounts/refresh-token", view_func=refresh_token_view, methods=["POST"])
blueprint.add_url_rule("/accounts/revoke-token", view_func=revoke_token_view, methods=["POST"])
blueprint.add_url_rule("/accounts/verify-email", view_func=verify_email_view, methods=["POST"])
blueprint.add_url_rule("/accounts/forgot-password", view_func=forgot_password_view, methods=["POST"])
blueprint.add_url_rule("/accounts/reset-password", view_func=reset_password_view, methods=["POST"])
blueprint.add_url_rule("/accounts/validate-reset-token", view_func=validate_reset_token_view, methods=["POST"])
blueprint.add_url_rule("/accounts", view_func=accounts_view, methods=["GET", "POST"])
blueprint.add_url_rule("/accounts/<account_id>", view_func=account_view, methods=["PUT", "GET", "DELETE"])
