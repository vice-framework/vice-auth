# -*- coding: utf-8 -*-
"""Auth models."""
import datetime as dt
import enum
import secrets
from calendar import timegm
from typing import Any, Dict, Mapping, Optional

from flask_jwt_extended import decode_token
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref
from sqlalchemy.orm.exc import NoResultFound

from vauth.database import Column, PkModel, Relationship, db, reference_col
from vauth.extensions import bcrypt

# TODO: Pylint should fix the ubsubscriptable bug eventually
# pylint: disable=unsubscriptable-object


class TokenType(enum.Enum):
    """Token type enumeration."""

    VERIFICATION = "verification"
    RESET = "reset"

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<{self.__class__.__name__}.{self.name}>"


class WebTokenType(enum.Enum):
    """Web token type enumeration."""

    ACCESS = "access"
    REFRESH = "refresh"

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"{self.__class__.__name__}, {self.name}"


class RevokeCause(enum.Enum):
    """Token type enumeration."""

    USER = "user"
    REFRESH = "refresh"

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<{self.__class__.__name__}.{self.name}>"


role_association_table = db.Table(
    "role_association",
    PkModel.metadata,
    reference_col("accounts", nullable=False, column_kwargs={"name": "left_id"}),
    reference_col("roles", nullable=False, column_kwargs={"name": "right_id"}),
)


class Role(PkModel):
    """Admin defined user roles."""

    __tablename__ = "roles"
    name = Column(db.Text, unique=True, nullable=False)
    accounts = Relationship("Account", back_populates="roles", lazy="dynamic", secondary=role_association_table)

    def __init__(self, name: str, **kwargs):
        """Create instance."""
        super().__init__(name=name, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        # pylint: disable=no-member
        return self._repr(id=self.id, name=self.name, accounts=[account.id for account in self.accounts.all()])


class Account(PkModel):
    """A auth of the app."""

    __tablename__ = "accounts"

    # Relationships
    roles = Relationship("Role", back_populates="accounts", lazy="dynamic", secondary=role_association_table)
    web_tokens = Relationship(
        "WebToken", cascade="all,delete", back_populates="account", lazy="dynamic", foreign_keys="WebToken.account_id"
    )
    tokens = Relationship(
        "Token", cascade="all,delete", back_populates="account", lazy="dynamic", foreign_keys="Token.account_id"
    )

    # Non-nullable
    email = Column(db.String(80), unique=True, nullable=False)
    created_date = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    updated_date = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    first_name = Column(db.String(30), nullable=False)
    last_name = Column(db.String(30), nullable=False)
    enabled = Column(db.Boolean, default=True)
    admin = Column(db.Boolean, default=False)

    # Nullable
    #: The hashed password
    password = Column(db.LargeBinary(128), nullable=True)
    verified_date = Column(db.DateTime, nullable=True)
    password_reset_date = Column(db.DateTime, nullable=True)

    def __init__(self, email: str, password: Optional[str] = None, **kwargs):
        """
        Create an Account instance.

        Args:
            email: Account email address
            password: Account clear text password
            **kwargs: Other Account information
        """
        super().__init__(email=email, **kwargs)
        if password:
            self.set_password(password)
        else:
            self.password = None

    def set_password(self, password: str) -> None:
        """
        Encrypt and store password.

        Args:
            password: Unhashed password

        Returns:
            None
        """
        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, password: str) -> bool:
        """
        Check unencrypted password against encrypted store.

        Args:
            password: Unhashed password

        Returns:
            True if password matches
        """
        return bcrypt.check_password_hash(self.password, password)

    @hybrid_property
    def full_name(self) -> str:
        """
        Retrieve the account user's full name.

        Returns:
            User's full name
        """
        return f"{self.first_name} {self.last_name}"

    def __repr__(self) -> str:
        """Represent instance as a unique string."""
        # pylint: disable=no-member
        return self._repr(
            id=self.id,
            email=self.email,
            first_name=self.first_name,
            last_name=self.last_name,
            admin=self.admin,
            roles=self.roles.all(),
            verified_date=str(self.verified_date),
        )


class MetaToken(PkModel):
    """Common token metadata."""

    __tablename__ = "meta_tokens"

    # Foreign keys
    revoker_id = reference_col("accounts", nullable=True)

    # Relationships
    revoker = Relationship("Account", lazy=True, foreign_keys=[revoker_id])

    # Non-Nullable
    revoked = db.Column(db.Boolean, nullable=False, default=False)
    expires_date = db.Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    issue_date = db.Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)

    # Nullable
    poly_type = Column(db.String(50))
    revoked_date = db.Column(db.DateTime)
    revoker_ip = db.Column(db.String(32))

    # Set polymorphic column
    __mapper_args__ = {"polymorphic_identity": "meta_tokens", "polymorphic_on": poly_type}

    @hybrid_property
    def expired(self) -> bool:
        """JWT expired status."""
        now = dt.datetime.utcnow()
        return self.expires_date < now

    @hybrid_property
    def valid(self) -> bool:
        """
        Token is valid if it is not expired nor revoked.

        Returns:
            True if token is valid
        """
        return not (self.expired or self.revoked)

    @valid.expression  # type: ignore
    def valid(self) -> int:
        """
        Token is valid if it is not expired nor revoked.

        SQLAlchemy version of this property.

        Returns:
            True if token is valid
        """
        return ~(self.expired | self.revoked)


class Token(MetaToken):
    """General-use tokens (e.g. reset and email verification)."""

    __tablename__ = "tokens"
    # Foreign keys
    # TODO: make use of the new revoker/issue date audit trail from MetaToken
    id = reference_col("meta_tokens", column_kwargs={"primary_key": True})
    account_id = reference_col("accounts", nullable=False, foreign_key_kwargs={"ondelete": "CASCADE"})

    # Relationships
    account = Relationship("Account", back_populates="tokens", lazy=True, foreign_keys=[account_id])

    # Non-Nullable
    tid = db.Column(db.String(40), nullable=False, default=secrets.token_urlsafe(39))
    type = db.Column(db.Enum(TokenType), nullable=False)

    # Ensure there can only be one type of token per account
    __table_args__ = (db.UniqueConstraint("account_id", "type", name="_account_type_uc"),)

    __mapper_args__ = {
        "polymorphic_identity": "tokens",
    }

    def __repr__(self) -> str:
        """Represent instance as a unique string."""
        # pylint: disable=no-member
        return self._repr(
            id=self.id,
            tid=self.tid,
            account_id=self.account_id,
            revoker_id=self.revoker_id,
            revoker_ip=self.revoker_ip,
            type=self.type,
            revoked=self.revoked,
            expires=str(self.expires_date),
        )


class WebToken(MetaToken):
    """JWT Token Information."""

    __tablename__ = "web_tokens"

    # Foreign keys
    id = reference_col("meta_tokens", column_kwargs={"primary_key": True})
    account_id = reference_col("accounts", nullable=False, foreign_key_kwargs={"ondelete": "CASCADE"})
    next_id = reference_col("web_tokens", nullable=True)

    # Relationships
    account = Relationship("Account", back_populates="web_tokens", lazy=True, foreign_keys=[account_id])
    next = Relationship(
        "WebToken", backref=backref("prev", lazy=True), remote_side="WebToken.id", lazy=True, foreign_keys=[next_id]
    )

    # Non-Nullable
    jti = db.Column(db.String(36), nullable=False)
    fresh = db.Column(db.Boolean, nullable=False, default=False)
    type = db.Column(db.Enum(WebTokenType), nullable=False)
    not_before_date = db.Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)

    # Nullable
    revoke_cause = db.Column(db.Enum(RevokeCause), nullable=True)

    __mapper_args__ = {
        "polymorphic_identity": "web_tokens",
    }

    def to_jwt_keys(self) -> Dict:
        """
        Create a JWT dictionary from row.

        Returns:
            Dictionary of JWT components
        """
        # pylint: disable=no-member
        return {
            "exp": timegm(self.expires_date.utctimetuple()),
            "iat": timegm(self.issue_date.utctimetuple()),
            "jti": self.jti,
            "nbf": timegm(self.not_before_date.utctimetuple()),
            "sub": self.account.email,
            "type": self.type.value,
            "fresh": self.fresh,
        }

    @staticmethod
    def from_encoded_token(encoded_token: str) -> "WebToken":
        """
        Create ORM Token object from encoded token.

        Args:
            encoded_token: JWT encoded token

        Returns:
            Token row
        """
        decoded_token = decode_token(encoded_token)
        token = WebToken()
        token.jti = decoded_token["jti"]
        token.type = WebTokenType(decoded_token["type"])
        token.account_id = Account.query.filter_by(email=decoded_token["sub"]).one().id
        token.expires_date = dt.datetime.utcfromtimestamp(decoded_token["exp"])
        token.revoked = False

        return token

    @staticmethod
    def is_token_revoked(decoded_token: Mapping[str, Any]) -> bool:
        """
        Check if token has been revoked.

        Because we are adding all the tokens that we create into this database, if the token is not present in the
        database we are going to consider it revoked, as we don't know where it was created.

        Args:
            decoded_token: Decoded JWT token

        Returns:
            True if token is revoked
        """
        jti = decoded_token["jti"]
        try:
            token = WebToken.query.filter_by(jti=jti).one()
            return token.revoked
        except NoResultFound:
            return True

    def __repr__(self):
        """Represent instance as a unique string."""
        # pylint: disable=no-member
        return self._repr(
            id=self.id,
            jti=self.jti,
            account_id=self.account_id,
            revoker_id=self.revoker_id,
            revoker_ip=self.revoker_ip,
            revoke_cause=self.revoke_cause,
            next_id=self.next_id,
            type=self.type,
            revoked=self.revoked,
            expires=str(self.expires_date),
        )
