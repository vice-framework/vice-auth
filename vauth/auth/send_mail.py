"""Send various emails using SMTP."""
import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from flask import current_app

# Todo: send emails in different thread
from vauth.exceptions import ServiceUnavailableError


def send_verification_email(email, verification_token, origin):
    """
    Send a verification email to the specified user that contains an email verification token.

    Args:
        email: Email address
        verification_token: Email verification token
        origin: UI address

    Returns:
        None
    """
    message = MIMEMultipart("alternative")

    if origin is not None:
        verify_url = f'{origin}{current_app.config["UI_EMAIL_VERIFY_URI"]}?token={verification_token}'

        html = (
            "<p>Please click the below link to verify your email address:</p>"
            f'<p><a href="{verify_url}">{verify_url}</a></p>'
        )
    else:
        html = (
            "<p>Please use the below token to verify your email address with the "
            "<code>/account/verify-email</code> api route:</p>"
            f"<p><code>{verification_token}</code></p>"
        )

    # part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    # message.attach(part1)
    message.attach(part2)
    send_email(email, message, f'{current_app.config["PLATFORM_NAME"]} Registration Verification')


def send_reset_email(email: str, reset_token: str, origin: str) -> None:
    """
    Send an email to the specified user that contains a password reset token.

    Args:
        email: Email address
        reset_token: Password reset token
        origin: UI address

    Returns:
        None
    """
    message = MIMEMultipart("alternative")

    if origin is not None:
        reset_url = f'{origin}{current_app.config["UI_PASSWORD_RESET_URI"]}?token={reset_token}'

        html = (
            "<p>Please click the below link to reset your password, the link will be valid for 1 day:</p>"
            f'<p><a href="{reset_url}">{reset_url}</a></p>'
        )
    else:
        html = (
            "<p>Please use the below token to reset your password with the "
            "<code>/account/reset-password</code> api route:</p>"
            f"<p><code>{reset_token}</code></p>"
        )

    # Turn these into plain/html MIMEText objects
    # part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    # message.attach(part1)
    message.attach(part2)
    send_email(email, message, f'{current_app.config["PLATFORM_NAME"]} Reset Password')


def send_already_registered_email(email: str, origin: str) -> None:
    """
    Send an email to indicate that the user is already registered.

    Args:
        email: Email address
        origin: UI address

    Returns:
        None
    """
    message = MIMEMultipart("alternative")

    if origin is not None:
        html = (
            f"<p> If you don't know your password please visit the "
            f'<a href="{origin}/account/forgot-password">forgot password</a> page.</p>'
        )
    else:
        html = (
            "<p> If you don't know your password you can reset it via the "
            "<code>/account/forgot-password</code> api route.</p>"
        )

    # Turn these into plain/html MIMEText objects
    # part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    # message.attach(part1)
    message.attach(part2)
    send_email(email, message, f'{current_app.config["PLATFORM_NAME"]} Email Already Registered')


def send_email(email: str, message: MIMEMultipart, subject: str) -> None:
    """
    Send an email with the given message and subject.

    Args:
        email: Email address
        message: Email message
        subject: Subject of the email

    Returns:
        None
    """
    if not current_app.config["SMTP_ENABLE"]:
        raise ServiceUnavailableError("SMTP is not enabled. Ask your admin to create a reset token your password.")

    message["Subject"] = subject
    message["From"] = current_app.config["SMTP_USERNAME"]
    message["To"] = email

    context = ssl.create_default_context()
    with smtplib.SMTP(current_app.config["SMTP_HOST"], current_app.config["SMTP_PORT"]) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(current_app.config["SMTP_USERNAME"], current_app.config["SMTP_PASSWORD"])
        server.sendmail(current_app.config["SMTP_USERNAME"], email, message.as_string())
