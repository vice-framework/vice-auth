"""Logic implementation for auth service."""
import datetime as dt
from typing import Dict, List, Tuple

from flask import current_app
from flask_jwt_extended import create_access_token, create_refresh_token
from sqlalchemy import true
from sqlalchemy.orm.exc import NoResultFound

from vauth.auth import send_mail
from vauth.auth.models import Account, RevokeCause, Token, TokenType, WebToken, WebTokenType
from vauth.exceptions import AlreadyExists, AuthenticationError, AuthorizationError, ForbiddenError, NotFound
from vauth.extensions import db


def serialize_account(account: Account) -> Dict:
    """
    Serialize the Account object suitable for API GET requests.

    Args:
        account: `Account` object to serialize

    Returns:
        Account object as a dictionary
    """
    # TODO: replace with proper serializer, e.g. marshmallow
    return {
        "id": account.id,
        "title": "",
        "firstName": account.first_name,
        "lastName": account.last_name,
        "email": account.email,
        "role": "Admin" if account.admin else "User",
        "created": account.created_date,
        "isVerified": bool(account.verified_date),
    }


def authenticate(email: str, password: str, remote_addr: str) -> Tuple[str, str, Dict]:
    """
    Create refresh token, only one refresh token can be valid at any time.

    Raises:
        `AuthenticationError` if account doesn't exist or password is invalid

    Returns:
        refresh token, access_token, account info
    """
    try:
        account = Account.query.filter_by(email=email).one()
    except NoResultFound as ex:
        raise AuthenticationError("Bad login.") from ex

    if not account.check_password(password):
        raise AuthenticationError("Bad login.")

    if account.enabled is False:
        raise AuthorizationError("Account has been disabled.")

    return refresh_token(email, remote_addr)


def verify_email(tid: str) -> None:
    """
    Verify that email exists by validating a verify-token.

    Args:
        tid: Email verify token

    Raises:
        `NotFound` if the account cannot be found.

    Returns:
        None
    """
    try:
        token = Token.query.filter_by(tid=tid).one()

        if token.expired:
            raise AuthenticationError("Token has expired.")

        if token.revoked:
            raise ForbiddenError("Token has been revoked.")

        token.account.update(verified=dt.datetime.utcnow())
        token.delete(commit=True)
    except NoResultFound as ex:
        raise NotFound("Could not verify email.") from ex


def forgot_password(email: str, origin: str) -> None:
    """
    Create a reset token and send email if the account exists.

    If request is made from a UI, the confirmation email sends a link to the UI URL,
    otherwise provides a raw token with instructions for verifying against the API.

    Args:
        email: Account email address
        origin: Origin of request

    Raises:
        `ServiceUnavailableError` if SMTP is not enabled.

    Returns:
        None
    """
    try:
        account = Account.query.filter_by(email=email).one()
    except NoResultFound:
        # Always return ok response to prevent email enumeration
        return

    if (old_token := Token.query.filter_by(type=TokenType.RESET).first()) is not None:
        # Prevent spamming
        if old_token.valid:
            return

        old_token.delete()

    token = Token(type=TokenType.RESET, expires_date=dt.datetime.utcnow() + current_app.config["RESET_PASSWORD_EXPIRY"])
    account.tokens.append(token)
    account.save()

    send_mail.send_reset_email(email, token.tid, origin)


def reset_password(tid: str, password: str) -> None:
    """
    Use reset token to replace account password.

    Args:
        tid: Password reset token
        password: New account password

    Returns:
        None
    """
    validate_reset_token(tid)

    try:
        account = Account.query.join(Account.tokens, aliased=True).filter_by(tid=tid, type=TokenType.RESET).one()
    except NoResultFound as ex:
        raise NotFound("Invalid reset token.") from ex

    account.set_password(password)
    account.password_reset_date = dt.datetime.utcnow()
    account.save(commit=True)

    Token.query.filter_by(tid=tid).delete()
    db.session.commit()


def create_account(email: str, password: str, first_name: str, last_name: str) -> Account:
    """
    Create a user account.

    Args:
        email: Account email addr
        password: Account password
        first_name: User first name
        last_name: User last name

    Raises:
        `AlreadyExists` if account already exists.

    Returns:
        Account object
    """
    if Account.query.filter_by(email=email).scalar() is not None:
        raise AlreadyExists(f"Email {email} is already registered.")

    account = Account.create(
        email=email, password=password, first_name=first_name, last_name=last_name, verified_date=dt.datetime.utcnow()
    )

    return account


def validate_reset_token(tid: str) -> None:
    """
    Validate an account reset token.

    Args:
        tid: Token to validate

    Raises:
        `NotFound` if the token doesn't exist.
        `AuthenticationError` if the token has expired.
        `ForbiddenError` if the token has been revoked.

    Returns:
        None
    """
    try:
        token = Token.query.filter_by(tid=tid, type=TokenType.RESET).one()
    except NoResultFound as ex:
        raise NotFound("Invalid reset token.") from ex

    if token.expired:
        raise AuthenticationError("Token has expired.")

    if token.revoked:
        raise ForbiddenError("Token has been revoked.")


def require_admin(requester: str) -> None:
    """
    Raise an authorization or authentication error if user isn't an admin or doesn't exist respectively.

    Args:
        requester: User to validate

    Raises:
        `AuthenticationError` if user does not exist
        `AuthorizationError` if user is not an admin

    Returns:
        None
    """
    try:
        account = Account.query.filter_by(email=requester).one()

        if not account.admin:
            raise AuthorizationError("Account is not authorized to use this API.")
    except NoResultFound as ex:
        raise AuthenticationError("Must be admin to view other accounts.") from ex


def get_admin_status(requester: str) -> bool:
    """
    Determine if requester is an admin.

    Args:
        requester: user email to check

    Raises:
        `NotFound` if the account can not be found.

    Returns:
        Admin status
    """
    try:
        return Account.query.filter_by(email=requester).one().admin
    except NoResultFound as ex:
        raise NotFound("User could not be identified.") from ex


def get_accounts() -> List[Account]:
    """
    Retrieve all current accounts.

    Returns:
        List of current accounts
    """
    return Account.query.all()


def update_account(account_id: int, account_data: Dict) -> Account:
    """
    Update account information.

    Args:
        account_id: ID of account to update
        account_data: New attributes to apply

    Returns:
        SQLAlchemy object
    """
    account = get_account_by_id(account_id)

    password = account_data.pop("password", None)
    account.update(commit=False, **account_data)

    if password is not None:
        account.set_password(password)
        account.save(commit=False)

    account.update(updated=dt.datetime.utcnow())

    return account


def get_account_by_id(account_id: int) -> Account:
    """
    Retrieve account information by ID.

    Requires user to own account or be an admin.

    Args:
        account_id: Account ID

    Returns:
        User account
    """
    account = Account.get_by_id(account_id)

    if account is None:
        raise NotFound("Could not find account.")

    return account


def refresh_token(requester: str, remote_addr: str) -> Tuple[str, str, Dict]:
    """
    Refresh both access and refresh tokens.

    Args:
        requester: User requesting fresh tokens
        remote_addr: Address of requesting user

    Returns:
        refresh token, access token, and account data
    """
    # TODO: don't revoke access tokens by default, multiple tokens could be in play with various targets
    # Revoke all valid tokens
    account = Account.query.filter_by(email=requester).one()
    tokens = account.web_tokens.filter(WebToken.valid == true()).all()

    # Perform refresh token rotation TODO: do this better with JWS
    _refresh_token = create_refresh_token(identity=requester)
    new_refresh_token = WebToken.from_encoded_token(_refresh_token).save()

    access_token = create_access_token(identity=requester)
    new_access_token = WebToken.from_encoded_token(access_token).save()

    for token in tokens:
        new_token = new_refresh_token if token.type == WebTokenType.REFRESH else new_access_token
        token.update(
            revoked=True, revoker_ip=remote_addr, revoker=account, next=new_token, revoke_cause=RevokeCause.REFRESH
        )

    return _refresh_token, access_token, serialize_account(account)


def register(email: str, password: str, first_name: str, last_name: str, origin: str) -> None:
    """
    Register a user account and issue email verification.

    Attempts to block account enumeration errors.
    """
    token = Token(
        type=TokenType.VERIFICATION, expires_date=dt.datetime.utcnow() + current_app.config["VERIFY_EMAIL_EXPIRY"]
    )

    try:
        account = create_account(email, password, first_name, last_name)

        if account.id == 1:
            account.update(admin=True, commit=False)

        account.update(tokens=[token])

    except AlreadyExists:
        send_mail.send_already_registered_email(email, origin)
        return

    send_mail.send_verification_email(account.email, token.tid, origin)


def revoke_token(requester: str, remote_addr: str) -> None:
    """
    Revoke current user's tokens.

    Args:
        requester: User requesting operation
        remote_addr: Address of source of request

    Returns:
        None
    """
    # TODO: should allow admins to revoke non-owned tokens
    account = Account.query.filter_by(email=requester).one()
    db_tokens = account.web_tokens.filter(WebToken.valid == true()).all()

    for token in db_tokens:
        token.update(revoked=True, revoker_ip=remote_addr, revoker=account, revoke_cause=RevokeCause.USER, commit=False)

    db.session.commit()


def delete_account(account_id: int) -> None:
    """
    Delete a given account ID.

    Args:
        account_id: Account to delete

    Returns:
        None
    """
    account = get_account_by_id(account_id)

    account.delete()
