# -*- coding: utf-8 -*-
"""Database module, including the SQLAlchemy database object and DB-related utilities."""
from typing import Any, Mapping, Optional, TypeVar, Union

import sqlalchemy as sa

from .extensions import db

# TODO: Pylint should fix the ubsubscriptable bug eventually
# pylint: disable=unsubscriptable-object

# Alias common SQLAlchemy names
Column = db.Column
Relationship = db.relationship

# Typing definitions
PK = TypeVar("PK", bound="PkModel")


class CRUDMixin:
    """Mixin that adds convenience methods for CRUD (create, read, update, delete) operations."""

    def __init__(self, **kwargs):
        """Keep linter happy, dummy constructor."""

    @classmethod
    def create(cls, **kwargs):
        """Create a new record and save it the database."""
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit: bool = True, **kwargs):
        """Update specific fields of a record."""
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return self.save() if commit else self

    def save(self, commit: bool = True):
        """Save the record."""
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit: bool = True):
        """Remove the record from the database."""
        db.session.delete(self)
        return commit and db.session.commit()


class Model(db.Model, CRUDMixin):
    """Base model class that includes CRUD convenience methods."""

    __abstract__ = True


class PkModel(Model):
    """Base model class that includes CRUD convenience methods, plus adds a 'primary key' column named ``id``."""

    __abstract__ = True
    id = Column(db.Integer, primary_key=True)

    @classmethod
    def get_by_id(cls, record_id: Union[str, bytes, int, float]) -> Optional[PK]:
        """Get record by ID."""
        if any(
            (
                isinstance(record_id, (str, bytes)) and record_id.isdigit(),
                isinstance(record_id, (int, float)),
            )
        ):
            return cls.query.get(int(record_id))
        return None

    def __repr__(self) -> str:
        """Represent instance as a unique string."""
        return self._repr(id=self.id)

    def _repr(self, **fields: Any) -> str:
        """Representation formatting."""
        field_strings = []
        at_least_one_attached_attribute = False
        for key, field in fields.items():
            try:
                field_strings.append(f"{key}={field!r}")
            except sa.orm.exc.DetachedInstanceError:
                field_strings.append(f"{key}=DetachedInstanceError")
            else:
                at_least_one_attached_attribute = True
        if at_least_one_attached_attribute:
            return f"<{self.__class__.__name__}({','.join(field_strings)})>"
        return f"<{self.__class__.__name__} {id(self)}>"


def reference_col(
    tablename: str,
    nullable: bool = False,
    pk_name: str = "id",
    foreign_key_kwargs: Optional[Mapping] = None,
    column_kwargs: Optional[Mapping] = None,
) -> db.Column:
    """
    Column that adds primary key foreign key reference.

    Args:
        tablename: Name of the table to add a reference to
        nullable: True if nullable
        pk_name: Name of the primary key
        foreign_key_kwargs: Extra arguments for SQLAlchemy ForeignKey
        column_kwargs: Extra arguments for SLQAlchemy Column

    Examples:
        category_id = reference_col('category')
        category = relationship('Category', backref='categories')

    Returns:
        SQLAlchemy Column
    """
    foreign_key_kwargs = foreign_key_kwargs or {}
    column_kwargs = column_kwargs or {}

    return Column(
        db.ForeignKey(f"{tablename}.{pk_name}", **foreign_key_kwargs),
        nullable=nullable,
        **column_kwargs,
    )
