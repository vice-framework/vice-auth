"""Custom exceptions for vauth."""
from werkzeug.exceptions import HTTPException


class AlreadyExists(HTTPException):
    """Database entity already exists."""

    code = 409
    description = "Database entity already exists."


class AuthenticationError(HTTPException):
    """Could not be authenticated."""

    code = 401
    description = "Could not be authenticated."


class AuthorizationError(HTTPException):
    """Could not be authorized."""

    code = 401
    description = "Could not be authorized."


class NotFound(HTTPException):
    """Could not find requested entity."""

    code = 404
    description = "Could not find requested entity."


class BadRequest(HTTPException):
    """Bad or malformed request."""

    code = 400
    description = "Bad or malformed request."


class RequestTimeout(HTTPException):
    """Request Timeout."""

    code = 408
    description = "Request timeout."


class ForbiddenError(HTTPException):
    """Request Timeout."""

    code = 403
    description = "Forbidden."


class ServiceUnavailableError(HTTPException):
    """Request Timeout."""

    code = 503
    description = "Not implemented."
