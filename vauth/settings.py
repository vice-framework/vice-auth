# -*- coding: utf-8 -*-
"""Application configuration.

Most configuration is set via environment variables.

For local development, use a .env file to set
environment variables.
"""
from environs import Env

env = Env()
env.read_env()

ENV = env.str("FLASK_ENV", default="production")
DEBUG = ENV == "development"
SQLALCHEMY_DATABASE_URI = env.str("DATABASE_URL")
SECRET_KEY = env.str("SECRET_KEY")
BCRYPT_LOG_ROUNDS = env.int("BCRYPT_LOG_ROUNDS", default=13)
CACHE_TYPE = env.str("CACHE_TYPE", "simple")  # Can be "memcached", "redis", etc.
JWT_EXPIRY_TIME = env.int("JWT_EXPIRY_TIME", 15 * 60)  # Default is 15 minutes
SQLALCHEMY_TRACK_MODIFICATIONS = False
JWT_IDENTITY_CLAIM = "sub"
JWT_ERROR_MESSAGE_KEY = "message"
CORS_HEADERS = "Content-Type"

# TODO: check these JWT settings
JWT_TOKEN_LOCATION = ["cookies", "headers"]
JWT_REFRESH_COOKIE_NAME = "refreshToken"
JWT_COOKIE_CSRF_PROTECT = False

# General
UI_EMAIL_VERIFY_URI = env.str("UI_EMAIL_VERIFY_URI", "/account/verify-email")
UI_PASSWORD_RESET_URI = env.str("UI_PASSWORD_RESET_URI", "/account/reset-password")
PLATFORM_NAME = env.str("PLATFORM_NAME", "Vice")
VERIFY_EMAIL_EXPIRY = env.timedelta("VERIFY_EMAIL_EXPIRY", 3600 * 24)  # Environ expects time in seconds
RESET_PASSWORD_EXPIRY = env.timedelta("RESET_PASSWORD_EXPIRY", 3600 * 24)

# SMTP Settings
SMTP_ENABLE = env.bool("SMTP_ENABLE", False)
SMTP_HOST = env.str("SMTP_HOST", "")
SMTP_PORT = env.int("SMTP_PORT", 587)
SMTP_USERNAME = env.str("SMTP_USERNAME", "")
SMTP_PASSWORD = env.str("SMTP_PASSWORD", "")
