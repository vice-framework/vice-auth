# -*- coding: utf-8 -*-
"""The app module, containing the app factory function."""
import json
import logging
import sys

from flask import Flask
from werkzeug.exceptions import HTTPException

from vauth import auth
from vauth.auth.models import WebToken
from vauth.extensions import bcrypt, cache, cors, db, jwt, migrate


def create_app(config_object="vauth.settings") -> Flask:
    """
    Create application factory, as explained here: http://flask.pocoo.org/docs/patterns/appfactories/.

    Args:
        config_object: The configuration object to use.

    Returns:
        Configured app
    """
    app = Flask(__name__.split('.', maxsplit=1)[0])
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    register_errorhandlers(app)
    register_shellcontext(app)
    register_commands(app)
    configure_logger(app)
    configure_callbacks()
    return app


def register_extensions(app: Flask):
    """Register Flask extensions."""
    jwt.init_app(app)
    bcrypt.init_app(app)
    cache.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    cors.init_app(app, supports_credentials=True)


def register_blueprints(app: Flask):
    """Register Flask blueprints."""
    app.register_blueprint(auth.views.blueprint)


def register_errorhandlers(app: Flask):
    """Register error handlers."""
    # pylint: disable=unused-variable

    @app.errorhandler(HTTPException)
    def handle_exception(exception):
        """Return JSON instead of HTML for HTTP errors."""
        # start with the correct headers and status code from the error
        response = exception.get_response()
        # replace the body with JSON
        response.data = json.dumps({"code": exception.code, "name": exception.name, "message": exception.description})
        response.content_type = "application/json"
        return response


def register_shellcontext(app: Flask):
    """Register shell context objects."""

    def shell_context():
        """Shell context objects."""
        return {
            "db": db,
            "Account": auth.models.Account,
            "Token": auth.models.Token,
            "TokenType": auth.models.TokenType,
            "WebToken": auth.models.WebToken,
            "WebTokenType": auth.models.WebTokenType,
            "Role": auth.models.Role,
        }

    app.shell_context_processor(shell_context)
    app.config.update({"KONCH_IPY_AUTORELOAD": True})


def register_commands(app: Flask):  # pylint: disable=unused-argument
    """Register Click commands."""
    # app.cli.add_command(commands.test)
    # app.cli.add_command(commands.lint)


def configure_logger(app: Flask):
    """Configure loggers."""
    if not app.logger.handlers:
        handler = logging.StreamHandler(sys.stdout)
        app.logger.addHandler(handler)


def configure_callbacks():
    """Flask add-on callbacks."""
    # pylint: disable=unused-variable

    @jwt.token_in_blocklist_loader
    def check_if_token_revoked(_, jwt_payload):
        return WebToken.is_token_revoked(jwt_payload)
